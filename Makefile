# meta data
NAME := go-fizzbuzz
VERSION  := $(shell git describe --tags --abbrev=0)
REVISION := $(shell git rev-parse --short HEAD)
LDFLAGS  := -X 'main.version=$(VERSION)' \
            -X 'main.revision=$(REVISION)'
SRCS     := main.go

## Setup
setup:
	go get github.com/golang/lint/golint
	go get golang.org/x/tools/cmd/goimports
	go get github.com/Songmu/make2help/cmd/make2help

## Run Tests
test:
	go test -v ./...

## build binaries
bin/%: $(SRCS)
	go build -ldflags "$(LDFLAGS)" -o $@ $(SRCS)

## Install binaries
install:
	go install -ldflags "$(LDFLAGS)"

## Run development source
run:
	go run $(SRCS)

## Run development bin
do:
	./bin/$(NAME)

## Show help
help:
	@make2help $(MAKEFILE_LIST)

.PHONY: setup test install run do help

package main

import (
	"fmt"
)

// FizzBuzz 関数
func FizzBuzz(num int) string {
	if num == 0 {
		return fmt.Sprintf("%d", num)
	}
	if num%3 == 0 && num%5 == 0 {
		return "FizzBuzz"
	}
	if num%3 == 0 {
		return "Fizz"
	}
	if num%5 == 0 {
		return "Buzz"
	}
	return fmt.Sprintf("%d", num)
}

func main() {
	for i := 0; i < 100; i++ {
		fmt.Println(FizzBuzz(i))
	}
}

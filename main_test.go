package main

import "testing"

func TestFizzBuzz(t *testing.T) {
	tests := []struct {
		num  int
		want string
	}{
		{0, "0"},
		{1, "1"},
		{2, "2"},
		{3, "Fizz"},
		{4, "4"},
		{5, "Buzz"},
		{6, "Fizz"},
		{7, "7"},
		{15, "FizzBuzz"},
		{30, "FizzBuzz"},
	}

	for _, test := range tests {
		got := FizzBuzz(test.num)
		if got != test.want {
			t.Errorf("fizzBuzz(%d) returned %s, want %s", test.num, got, test.want)
		}
	}
}
